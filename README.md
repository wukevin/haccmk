View Wiki for performance reports

[Wiki](https://bitbucket.org/wukevin/haccmk/wiki/Home)

# Build instructions
Build hcc instructions here: https://bitbucket.org/multicoreware/hcc/wiki/Home

If you already have hcc installed in yoru system, need to export the environment variable HCC_PATH to the location of the bin/lib/include directories of hcc. Example:
```
#!bash
export HCC_PATH=/opt/hcc
```
**Or**
If you build from source, you can edit the Makefile and change the locations of HCC_CONFIG and CC. Example:

```
#!bash
HCC_PREFIX = (path-to-your-hcc-build-folder)/build
HCC_CONFIG = $(HCC_PREFIX)/build/Release/bin/hcc-config
CC := $(HCC_PREFIX)/compiler/bin/clang++
```

Run make clean before building for a new target
```
#!bash
make cpu # will build for cpu
make gpu # will build for Boltzmann-compatible hardware
```
Make sure to ```export LD_LIBRARY_PATH="/opt/hsa/lib"``` to link hsa libraries and runtime and run ./HACCmk


**Note: On a selective basis, you can choose prebuilt clang-3.5 from opensource to build a cpu version as follows,**

```
#!bash

* Install clang-3.5: sudo apt-get install clang-3.5 
* set PLAT = intel in Makefile
```


After completion, if both output_cpu.txt and output_gpu.txt exist, you can run ```python compare.py``` to compare the two outputs. The script will display the locations that have a mismatch up to 100 errors.