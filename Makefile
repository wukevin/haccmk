
# bgq, intel, hcc
PLAT = hcc

ifeq "$(PLAT)" "hcc"
HCC_PREFIX = $(HCC_PATH)
HCC_CONFIG = $(HCC_PREFIX)/bin/hcc-config
CC := $(HCC_PREFIX)/bin/clang++
LD = $(CC)
LIBS = -lm
HCC_COMMON_FLAGS = $(shell $(HCC_CONFIG) --build --cxxflags) -DPRINTVAL $(TARGET)
CFLAGS_KERNEL = -O3 $(HCC_COMMON_FLAGS)
CFLAGS_MAIN   = -O3 $(HCC_COMMON_FLAGS)
LINKFLAGS     = $(shell $(HCC_CONFIG) --build --ldflags)
LDFLAGS       = -O3 $(LINKFLAGS)
endif

ifeq "$(PLAT)" "intel"
HCC_PREFIX = $(HCC_PATH)
CC := /usr/bin/clang++-3.5
LD = $(CC)
LIBS = -lm
HCC_COMMON_FLAGS = -DPRINTVAL $(TARGET)
CFLAGS_KERNEL = -O3 $(HCC_COMMON_FLAGS)
CFLAGS_MAIN   = -O3 $(HCC_COMMON_FLAGS)
LDFLAGS       = -O3 $(LINKFLAGS)
endif

# not used if the target platform defined correctly
CFLAGS = -O2


OBJ = main.o Step10_orig.o mysecond.o

all: main

main.o : main.cpp
	$(CC) $(CFLAGS_MAIN) -c $< -o $@

mysecond.o : mysecond.cpp
	$(CC) -O3 -c -o mysecond.o mysecond.cpp

Step10_orig.o : Step10_orig.cpp
	$(CC) $(CFLAGS_KERNEL) -c $< -o $@

%.o : %.cpp
	$(CC) $(CFLAGS) -c $(INCLUDE) $<

%.o : %.s
	$(FF) $(FFLAGS) -c $(INCLUDE) $<

cpu : TARGET = -DUSE_CPU
cpu : main

gpu : main

main: $(OBJ)
	$(LD) $(LDFLAGS) -o HACCmk $(OBJ) $(LIBS)

clean:
	rm -f $(OBJ) HACCmk *.lst *.error *.cobaltlog

run: $(TARGET)$(EXE)
	./$(TARGET)$(EXE)


