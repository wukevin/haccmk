#ifndef USE_CPU
#include <hc.hpp>
#endif
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
//#include <omp.h>
//#include <mpi.h>

#ifdef USE_CPU
extern void Step10_orig( int count1, float xxi, float yyi, float zzi, float fsrrmax2, float mp_rsm2, float *xx1, float *yy1, float *zz1, float *mass1, float *dxi, float *dyi, float *dzi );
#else

#define TS_R 1
#define TS_C 256

#undef stRESULT
typedef struct _stRESULT {
  float res[TS_R];
} stRESULT;

#define _POW(a,b) hc::fast_math::pow(a,b)

// count: extent<2>: DIM0
// n:     extent<2>: DIM1
__attribute__((always_inline)) void Step10_orig( hc::tiled_index<2>& tidx, int count, int n, float fsrrmax2, float mp_rsm2,
    hc::array_view<float, 1> xx1, hc::array_view<float, 1> yy1, hc::array_view<float, 1> zz1, hc::array_view<float, 1> mass1, float fcoeff,
    hc::array_view<stRESULT, 2>& result_xis, hc::array_view<stRESULT, 2>& result_yis, hc::array_view<stRESULT, 2>& result_zis) __attribute((hc))
{
    int row = tidx.local[0];
    int col = tidx.local[1];
    int particle = tidx.global[0];
    int force = tidx.global[1];
    const float ma0 = 0.269327, ma1 = -0.0750978, ma2 = 0.0114808, ma3 = -0.00109313, ma4 = 0.0000605491, ma5 = -0.00000147177;

    tile_static float xis[TS_R][TS_C], yis[TS_R][TS_C], zis[TS_R][TS_C];

    if (particle < count && force < n) {
      float dxc = xx1[force] - xx1[particle];
      float dyc = yy1[force] - yy1[particle];
      float dzc = zz1[force] - zz1[particle];
      float r2 = dxc * dxc + dyc * dyc + dzc * dzc;
      float m = ( r2 < fsrrmax2 ) ? mass1[force] : 0.0f;
      float f =  _POW( r2 + mp_rsm2, -1.5 ) - ( ma0 + r2*(ma1 + r2*(ma2 + r2*(ma3 + r2*(ma4 + r2*ma5)))));
      f = ( r2 > 0.0f ) ? m * f : 0.0f;
      xis[row][col] = f * dxc;
      yis[row][col] = f * dyc;
      zis[row][col] = f * dzc;
    } else {
      xis[row][col] = 0.0f;
      yis[row][col] = 0.0f;
      zis[row][col] = 0.0f;
    }

    tidx.barrier.wait();

    // reduction
    for(int stride = TS_C/ 2; stride > 0; stride /= 2) {
      if (col < stride) {
        for (int k = 0; k < TS_R; k++) {
          xis[k][col] += xis[k][col + stride];
          yis[k][col] += yis[k][col + stride];
          zis[k][col] += zis[k][col + stride];
        }
      }

      tidx.barrier.wait();
    }

    if (row == 0 && col == 0) {
      for (int k = 0; k < TS_R; k++) {
        result_xis(tidx.tile).res[k] = xis[k][0] * fcoeff;
        result_yis(tidx.tile).res[k] = yis[k][0] * fcoeff;
        result_zis(tidx.tile).res[k] = zis[k][0] * fcoeff;
      }
    }
}

struct Step10_orig_functor {

  Step10_orig_functor(int count1, int n1,
    float fsrrmax2, float mp_rsm2,
    hc::array_view<float, 1> xx1, hc::array_view<float, 1> yy1, hc::array_view<float, 1> zz1, hc::array_view<float, 1> mass1,
    float fcoeff,
    hc::array_view<stRESULT, 2>& result_xis, hc::array_view<stRESULT, 2>& result_yis, hc::array_view<stRESULT, 2>& result_zis ) :

    count1(count1), n1(n1), fsrrmax2(fsrrmax2), mp_rsm2(mp_rsm2),
    xx1(xx1), yy1(yy1), zz1(zz1), mass1(mass1),
    fcoeff(fcoeff),
    result_xis(result_xis), result_yis(result_yis), result_zis(result_zis)
    { }

  void operator()(hc::tiled_index<2>& tidx) __attribute((hc)) {
    Step10_orig( tidx, count1, n1, fsrrmax2, mp_rsm2, xx1, yy1, zz1, mass1, fcoeff, result_xis, result_yis, result_zis );
  }

  int count1;
  int n1;
  float fsrrmax2; float mp_rsm2;
  hc::array_view<float, 1> xx1; hc::array_view<float, 1> yy1; hc::array_view<float, 1> zz1; hc::array_view<float, 1> mass1;
  float fcoeff;
  hc::array_view<stRESULT, 2> result_xis; hc::array_view<stRESULT, 2> result_yis; hc::array_view<stRESULT, 2> result_zis;
};
#endif

#ifdef TIMEBASE
extern unsigned long long timebase();
#else
extern double mysecond();
#endif

// Frequency in MHz, L1 cache size in bytes, Peak MFlops per node */
// BG/Q
#define MHz 1600e6
#define NC 16777216
#define PEAK (16.*12800.)

// 4 core Intel(R) Xeon(R) CPU E5-2643 0 @ 3.30GHz - Rick's machine celero
// Peak flop rate: AVX: 8 Flops/cycle * 4 cores * 3291.838 Mcycles/second
/*
#define MHz 3291.838e6
#define NC (64*1024*1024)
#define PEAK 105338.816
*/

// 4 core Intel(R) Xeon(R) CPU           E5430  @ 2.66GHz - crush
// Peak flop rate: SSE: 4 Flops/cycle * 4 cores * 2666.666 Mcycles/second
/*
#define MHz 2666.7e6
#define NC ( 256*1024*1024 )
#define PEAK 42667.2
*/

// 4 core Intel(R) Core(TM) i7-3820 CPU @ 3.60GHz - strength
// Peak flop rate: AVX: 8 Flops/cycle * 4 cores * 3600 MCycles/second
/*
#define MHz 3600.e6
#define NC ( 32*1024*1024 )
#define PEAK 115200.
*/

#define N 15000      /* Vector length, must be divisible by 4  15000 */

#define ETOL 1.e-4  /* Tolerance for correctness */

int main( int argc, char *argv[] )
{
#ifdef USE_CPU
  static float xx[N], yy[N], zz[N], mass[N], vx1[N], vy1[N], vz1[N];
#else
  hc::array_view<float, 1>   xx(hc::extent<1>(N));
  hc::array_view<float, 1>   yy(hc::extent<1>(N));
  hc::array_view<float, 1>   zz(hc::extent<1>(N));
  hc::array_view<float, 1> mass(hc::extent<1>(N));
  hc::array_view<float, 1>  vx1(hc::extent<1>(N));
  hc::array_view<float, 1>  vy1(hc::extent<1>(N));
  hc::array_view<float, 1>  vz1(hc::extent<1>(N));
#endif

  float fsrrmax2, mp_rsm2, fcoeff, dx1, dy1, dz1;

  char  M1[NC], M2[NC];
  int n, count, i, rank, nprocs;
#ifdef TIMEBASE
  unsigned long long tm1, tm2, tm3, tm4, total = 0;
#else
  unsigned long long t1, t2, total = 0;
#endif
  double t3, elapsed = 0.0, validation, final;

  //MPI_Init( &argc, &argv );
  //MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  //MPI_Comm_size( MPI_COMM_WORLD, &nprocs );

  rank = 0;
  nprocs = 1;

  count = 327;

  if ( rank == 0 )
  {
     printf( "count is set %d\n", count );
     printf( "Total MPI ranks %d\n", nprocs );
  }

/*
#pragma omp parallel
{
  if ( (rank == 0) && (omp_get_thread_num() == 0) )
  {
     printf( "Number of OMP threads %d\n\n", omp_get_num_threads() );
     //printf( "      N         Time,us        Validation result\n" );
  }
}
*/

#ifdef TIMEBASE
  tm3 = timebase();
#endif

  final = 0.;

  for ( n = 400; n < N; n = n + 20 )
  {
      /* Initial data preparation */
      fcoeff = 0.23f;
      fsrrmax2 = 0.5f;
      mp_rsm2 = 0.03f;
      dx1 = 1.0f/(float)n;
      dy1 = 2.0f/(float)n;
      dz1 = 3.0f/(float)n;
      xx[0] = 0.f;
      yy[0] = 0.f;
      zz[0] = 0.f;
      mass[0] = 2.f;

      for ( i = 1; i < n; i++ )
      {
          xx[i] = xx[i-1] + dx1;
          yy[i] = yy[i-1] + dy1;
          zz[i] = zz[i-1] + dz1;
          mass[i] = (float)i * 0.01f + xx[i];
      }

      for ( i = 0; i < n; i++ )
      {
          vx1[i] = 0.f;
          vy1[i] = 0.f;
          vz1[i] = 0.f;
      }

      /* Data preparation done */


      /* Clean L1 cache */
      for ( i = 0; i < NC; i++ ) M1[i] = 4;
      for ( i = 0; i < NC; i++ ) M2[i] = M1[i];

#ifdef TIMEBASE
      tm1 = timebase();
#else
      t1 = mysecond();
#endif

#ifdef USE_CPU
    #pragma omp parallel for private( dx1, dy1, dz1 )
      for ( i = 0; i < count; ++i)
      {
        Step10_orig( n, xx[i], yy[i], zz[i], fsrrmax2, mp_rsm2, xx, yy, zz, mass, &dx1, &dy1, &dz1 );

        vx1[i] = vx1[i] + dx1 * fcoeff;
        vy1[i] = vy1[i] + dy1 * fcoeff;
        vz1[i] = vz1[i] + dz1 * fcoeff;
      }
#else

// FIXME:  (1) TS_R > 1 is problematic. Possible reason is that array in a struct, e.g. stRESULT is not working in HC
// FIXME:  (2) (TS_R * TS_C) > 32*32 is problematic
    hc::extent<2> grdExt((count + (TS_R - 1)) & ~(TS_R - 1), (n + (TS_C - 1)) & ~(TS_C - 1));
    int tile_dim0 = grdExt[0] / TS_R;
    int tile_dim1 = grdExt[1] / TS_C;

    hc::array_view<stRESULT, 2> result_xs(tile_dim0, tile_dim1);
    hc::array_view<stRESULT, 2> result_ys(tile_dim0, tile_dim1);
    hc::array_view<stRESULT, 2> result_zs(tile_dim0, tile_dim1);

    hc::parallel_for_each(grdExt.tile(TS_R, TS_C),
            Step10_orig_functor( count, n, fsrrmax2, mp_rsm2, xx, yy, zz, mass, fcoeff, result_xs, result_ys, result_zs)).wait();

    for(int I = 0; I < tile_dim0; ++I) {
      for (int J = 0; J < tile_dim1; ++J) {
        for (int particle = I * TS_R; particle < I * TS_R + TS_R && particle < count; particle++) {
          vx1[particle] += result_xs[I][J].res[particle - I * TS_R];
          vy1[particle] += result_ys[I][J].res[particle - I * TS_R];
          vz1[particle] += result_zs[I][J].res[particle - I * TS_R];
        }
      }
   }
#endif // USE_CPU

#ifdef TIMEBASE
      tm2 = timebase();
#else
      t2 = mysecond();
#endif

      validation = 0.;
      for ( i = 0; i < n; i++ )
      {
         validation = validation + ( vx1[i] + vy1[i] + vz1[i] );
      }

      final = final + validation;

#ifdef TIMEBASE
      t3 = 1e6 * (double)(tm2 - tm1) / MHz; // time in us
#else
      t3 = (t2 - t1) * 1e6;
#endif

      elapsed = elapsed + t3;

  }

#ifdef TIMEBASE
  tm4 = timebase();
  if ( rank == 0 )
  {
      printf( "\nKernel elapsed time, s: %18.8lf\n", elapsed*1e-6 );
      printf(   "Total  elapsed time, s: %18.8lf\n", (double)(tm4 - tm3) / MHz );
      printf(   "Result validation: %18.8lf\n", final );
      printf(   "Result expected  : 6636045675.12190628\n" );
  }
#else
  if ( rank == 0 )
  {
      printf( "\nKernel elapsed time, s: %18.8lf\n", elapsed*1e-6 );
  }
#endif

  //MPI_Finalize();

#ifdef PRINTVAL
        std::ofstream myfile;
  #ifdef USE_CPU
        myfile.open("out_cpu.txt");
  #else
        myfile.open("out_gpu.txt");
  #endif
        for(int i = 0; i < N; ++i)
          myfile << vx1[i] << ", ";
        myfile << "\n";
        for(int i = 0; i < N; ++i)
          myfile << vy1[i] << ", ";
        myfile << "\n";
        for(int i = 0; i < N; ++i)
          myfile << vz1[i] << ", ";
        myfile << "\n";
        myfile.close();
#endif

  return 0;
}

