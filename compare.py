def areFloats(f1, f2):
    try:
        float(f1)
        float(f2)
        return True
    except ValueError:
        return False

def isclose(a, b, rel_tol=1e-04, abs_tol=0.0):
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

try:
    input_cpu = open("out_cpu.txt", "r")
except:
    raise ValueError("out_cpu.txt not found. Please rerun cpu version.")
try:
    input_gpu = open("out_gpu.txt", "r")
except:
    raise ValueError("out_gpu.txt not found. Please rerun gpu version.")

list_cpu = []
list_gpu = []

for line in input_cpu:
    list_cpu.append(line.split(','))

for line in input_gpu:
    list_gpu.append(line.split(','))

numArrays = len(list_cpu)
arrayLen = len(list_cpu[0])

# quick comparison between two sizes
if numArrays != len(list_gpu):
    raise ValueError('cpu and gpu number of outputs mismatch', numArrays, "vs", len(list_gpu))
if arrayLen != len(list_gpu[0]):
    raise ValueError('cpu and gpu length of first output mismatch', arrayLen, "vs", len(list_gpu[0]))

# maximum error count to limit the number of errors flooding the terminal
maxerrcount = 100
numerrs = 0

print "mismatches (max", maxerrcount, " errors):"
print "(array number, index of array, cpu value, !=, gpu value)"
for A in range(numArrays):
    cpu = list_cpu[A]
    gpu = list_gpu[A]
    for i in range(arrayLen):
        if areFloats(cpu[i], gpu[i]):
            if not isclose(float(cpu[i]), float(gpu[i])):
                numerrs += 1
                if numerrs < maxerrcount:
                    print(A, i, cpu[i], "!=", gpu[i])
